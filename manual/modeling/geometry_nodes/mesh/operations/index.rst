
########################
  Mesh Operation Nodes
########################

Nodes that only operate on meshes.

.. toctree::
   :maxdepth: 1

   dual_mesh.rst
   edge_paths_to_curves.rst
   edge_paths_to_selection.rst
   extrude_mesh.rst
   flip_faces.rst
   mesh_boolean.rst
   mesh_to_curve.rst
   mesh_to_points.rst
   mesh_to_volume.rst
   scale_elements.rst
   split_edges.rst
   subdivide_mesh.rst
   subdivision_surface.rst
   triangulate.rst
