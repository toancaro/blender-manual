
################
  Curves (New)
################

.. toctree::
   :maxdepth: 2

   tools/index.rst
   primitives.rst
   selecting.rst
   editing/index.rst
   properties.rst
