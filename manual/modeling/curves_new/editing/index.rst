
###########
  Editing
###########

The curves can be edited via :doc:`sculpting </sculpt_paint/curves_sculpting/introduction>`.

Curves objects also have basic editing support in "Edit Mode".

.. toctree::
   :maxdepth: 2

   curves.rst
   control_points.rst
